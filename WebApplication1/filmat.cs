﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Data.SqlClient;
using REST_FILM.Models;
using System.Data;
using System.Collections;


namespace REST_FILM
{

    public class filmat
    {
        private string connectionString = @"Data Source=ERILDI\ERILDI;Initial Catalog=Company;Integrated Security=True";
        private SqlConnection con;
        public filmat()
        {

            using (con = new SqlConnection(connectionString))
            {

                con.Open();
            }

        }



        public ArrayList getfilmat()
        {

            ArrayList arr = new ArrayList();
            con = new SqlConnection(connectionString);
            string sql = "SELECT * FROM film as json";
            SqlCommand cmd = new SqlCommand(sql, con);
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                film f = new film();
                f.id = reader.GetInt32(0);
                f.titull = reader.GetString(1);
                f.vit = reader.GetInt32(2);
                f.regjisor = reader.GetString(3);
                f.aktor = reader.GetString(4);
                arr.Add(f);
            }
            return arr;
        }


        public ArrayList getfilm(int id)
        {
            ArrayList arr = new ArrayList();
            film f = new film();
            con = new SqlConnection(connectionString);
            string sql = "SELECT * FROM film WHERE id=@id";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                f.id = reader.GetInt32(0);
                f.titull = reader.GetString(1);
                f.vit = reader.GetInt32(2);
                f.regjisor = reader.GetString(3);
                f.aktor = reader.GetString(4);
                arr.Add(f);

            }
            return arr;
        }

        public ArrayList kerkofilmVitRegj(int vit, string rendit)
        {
            ArrayList arr = new ArrayList();
            con = new SqlConnection(connectionString);
            string sql = "SELECT id FROM film WHERE vit=@vit order by ";
            sql += rendit;
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@vit", SqlDbType.Int).Value = vit;
            //cmd.Parameters.Add("@rendit", SqlDbType.Char).Value = rendit;
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                arr.Add(reader.GetInt32(0));
            }
            return arr;
        }


        public int ruajfilm(film film1)
        {
            film filmi = new film();
            filmi = film1;
            con = new SqlConnection(connectionString);
            con.Open();
            string sql = "INSERT INTO film (titull,vit,regjisor,aktor) VALUES ('" + filmi.titull + "','" + filmi.vit + "','" + filmi.regjisor + "','" + filmi.aktor + "');";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            return filmi.id;
        }
       
        public bool modifikofilm(int id, film film1)
        {
            film filmi = new film();
            filmi = film1;
            con = new SqlConnection(connectionString);
            con.Open();
            string kontrollo = "select * from film where id=@id1";
            SqlCommand cd = new SqlCommand(kontrollo, con);
            cd.Parameters.Add("@id1", SqlDbType.Int).Value = id;
            int n = Convert.ToInt32(cd.ExecuteScalar());
            if (n == 0)
            {
                string sql1 = "INSERT INTO film (titull,vit,regjisor,aktor) VALUES ('" + filmi.titull + "','" + filmi.vit + "','" + filmi.regjisor + "','" + filmi.aktor + "');";
                SqlCommand cmd1 = new SqlCommand(sql1, con);
                cmd1.ExecuteNonQuery();
                return true;
            }
            else
            {
                string sql = "UPDATE film Set titull='" + filmi.titull + "',vit='" + filmi.vit + "',regjisor='" + filmi.regjisor + "',aktor='" + filmi.aktor + "' WHERE id=@id";
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                int nr = cmd.ExecuteNonQuery();
                if (nr > 0)
                {

                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


        public bool fshifilm(int id)
        {
            film f = new film();
            con = new SqlConnection(connectionString);
            string sql = "DELETE FROM film WHERE id=@id";
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
            con.Open();
            int nr = cmd.ExecuteNonQuery();
            if (nr > 0)
            {

                return true;
            }
            else
            {
                return false;
            }
        }


    }

}