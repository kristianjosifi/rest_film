﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections;
using REST_FILM.Models;

namespace REST_FILM.Controllers
{
    public class filmatController : ApiController
    {
        // REST FILM/burimet/FilmDB/filmat
        public ArrayList Get()
        {
            filmat f = new filmat();
            return f.getfilmat();
        }

        // REST FILM/burimet/FilmDB/filmat/5
        public ArrayList Get(int id)
        {
            filmat f = new filmat();
            return f.getfilm(id);

        }

        public ArrayList Get(int vit, string rendit)
        {
            filmat f = new filmat();
            return f.kerkofilmVitRegj(vit, rendit);

        }


        // POST REST FILM/burimet/FilmDB/filmat
        public HttpResponseMessage Post([FromBody]film value)
        {
            filmat film1 = new filmat();
            int id = film1.ruajfilm(value);
            value.id = id;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            return response;
        }

        // PUT REST FILM/burimet/FilmDB/filmat/5
        public HttpResponseMessage Put(int id, [FromBody]film value)
        {
            filmat film1 = new filmat();
            bool mod = false;
            mod = film1.modifikofilm(id, value);
            HttpResponseMessage response;
            if (mod)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return response;

        }

        // DELETE REST FILM/burimet/FilmDB/filmat/5
        public HttpResponseMessage Delete(int id)
        {
            filmat film1 = new filmat();
            bool del = false;
            del = film1.fshifilm(id);
            HttpResponseMessage response;
            if (del)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return response;
        }
    }
}
